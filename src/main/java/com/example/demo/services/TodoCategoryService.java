package com.example.demo.services;

import com.example.demo.models.Todo;
import com.example.demo.models.TodoCategory;
import com.example.demo.repositories.TodoCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoCategoryService {

    @Autowired
    TodoCategoryRepository _repository;

    public List<TodoCategory> GetAllTodoCategory() {
        List<TodoCategory> listResult = new ArrayList<>();

        _repository.findAll().forEach(listResult::add);

        return listResult;
    }

    public TodoCategory GetTodoCategory(long todoCategoryId) {
        return _repository.findTodoCategoryById(todoCategoryId);
    }

    public void AddNewTodoCategory(TodoCategory todoCategoryData) {
        _repository.save(todoCategoryData);
    }

    public void DeleteTodoCategory(long todoCategoryId) {
        TodoCategory existCategory = _repository.findTodoCategoryById(todoCategoryId);

        _repository.delete(existCategory);
    }
}
