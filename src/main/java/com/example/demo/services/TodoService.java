package com.example.demo.services;

import com.example.demo.models.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.repositories.TodoRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoService {

    @Autowired
    TodoRepository _repository;

    public List<Todo> GetAllTodo() {
        List<Todo> listResult = new ArrayList<>();

        _repository.findAll().forEach(listResult::add);

        return listResult;
    }

    public Todo GetTodo(long todoId) {
        return _repository.findTodoById(todoId);
    }

    public void AddNewTodo(Todo todoData) {
        _repository.save(todoData);
    }

    public void MarkTodoDone(long todoId) {
        Todo existTodo = _repository.findTodoById(todoId);
        existTodo.setDone(true);

        _repository.save(existTodo);
    }

    public void DeleteTodo(long todoId) {
        Todo existTodo = _repository.findTodoById(todoId);

        _repository.delete(existTodo);
    }
}
