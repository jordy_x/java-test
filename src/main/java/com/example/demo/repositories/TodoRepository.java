package com.example.demo.repositories;

import com.example.demo.models.Todo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TodoRepository extends CrudRepository<Todo, Long> {

    @Query(value = "SELECT x.id, x.title, x.description, x.saved_date, x.is_done, x.category_id " +
            "FROM tbl_todo AS x " +
            "INNER JOIN tbl_todo_category AS y " +
            "ON x.category_id = y.id " +
            "WHERE x.id = :id " +
            "ORDER BY x.saved_date DESC", nativeQuery = true)
    Todo findTodoById(@Param("id") long id);

}
