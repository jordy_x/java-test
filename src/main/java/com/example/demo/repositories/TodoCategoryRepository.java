package com.example.demo.repositories;

import com.example.demo.models.Todo;
import com.example.demo.models.TodoCategory;
import org.springframework.data.repository.CrudRepository;

public interface TodoCategoryRepository extends CrudRepository<TodoCategory, Long> {

    TodoCategory findTodoCategoryById(long id);

}
