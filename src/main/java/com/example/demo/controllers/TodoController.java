package com.example.demo.controllers;

import com.example.demo.models.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.services.TodoService;

import java.util.List;

@RestController
public class TodoController {
    @Autowired
    private TodoService todoService;

    @GetMapping("/getalldata")
    public List<Todo> GetAllData() {
        return todoService.GetAllTodo();
    }

    @GetMapping("/getdata")
    public Todo GetData(@RequestParam(name = "id") long todoId) {
        return todoService.GetTodo(todoId);
    }

    @PostMapping("/add")
    public void AddTodoData(@RequestBody Todo todoData) {
        todoService.AddNewTodo(todoData);
    }

    @PostMapping("/done")
    public void MarkDone(@RequestParam(name = "id") long todoId) {
        todoService.MarkTodoDone(todoId);
    }

    @GetMapping("/delete")
    public void DeleteTodo(@RequestParam(name = "id") long todoId) {
        todoService.DeleteTodo(todoId);
    }
}
