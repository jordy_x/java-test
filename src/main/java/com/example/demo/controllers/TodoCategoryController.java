package com.example.demo.controllers;

import com.example.demo.models.Todo;
import com.example.demo.models.TodoCategory;
import com.example.demo.services.TodoCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoCategoryController {
    @Autowired
    private TodoCategoryService todoCategoryService;

    @GetMapping("/getallcategory")
    public List<TodoCategory> GetAllData() {
        return todoCategoryService.GetAllTodoCategory();
    }

    @GetMapping("/getcategory")
    public TodoCategory GetData(@RequestParam(name = "id") long todoId) {
        return todoCategoryService.GetTodoCategory(todoId);
    }

    @PostMapping("/addcategory")
    public void AddTodoData(@RequestBody TodoCategory todoData) {
        todoCategoryService.AddNewTodoCategory(todoData);
    }

    @GetMapping("/deletecategory")
    public void DeleteTodoCategory(@RequestParam(name = "id") long todoId) {
        todoCategoryService.DeleteTodoCategory(todoId);
    }
}
